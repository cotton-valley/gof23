package com.zwt.builders;

/**
 * @author ML李嘉图
 * @version createtime: 2021-11-11
 * Blog: https://www.cnblogs.com/zwtblog/
 */

import com.zwt.cars.CarType;
import com.zwt.components.Engine;
import com.zwt.components.GPSNavigator;
import com.zwt.components.Transmission;
import com.zwt.components.TripComputer;

/**
 * Builder interface defines all possible ways to configure a product.
 */
public interface Builder {
    void setCarType(CarType type);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTransmission(Transmission transmission);
    void setTripComputer(TripComputer tripComputer);
    void setGPSNavigator(GPSNavigator gpsNavigator);
}
