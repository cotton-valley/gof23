package com.zwt.cars;

/**
 * @author ML李嘉图
 * @version createtime: 2021-11-11
 * Blog: https://www.cnblogs.com/zwtblog/
 */


public enum CarType {
    CITY_CAR, SPORTS_CAR, SUV
}
