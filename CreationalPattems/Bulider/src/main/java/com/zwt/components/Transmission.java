package com.zwt.components;

/**
 * @author ML李嘉图
 * @version createtime: 2021-11-11
 * Blog: https://www.cnblogs.com/zwtblog/
 */


/**
 * Just another feature of a car.
 */
public enum Transmission {
    SINGLE_SPEED, MANUAL, AUTOMATIC, SEMI_AUTOMATIC
}
