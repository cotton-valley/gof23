package com.zwt.components;

/**
 * @author ML李嘉图
 * @version createtime: 2021-11-11
 * Blog: https://www.cnblogs.com/zwtblog/
 */


import com.zwt.cars.Car;

/**
 * Just another feature of a car.
 */
public class TripComputer {

    private Car car;

    public void setCar(Car car) {
        this.car = car;
    }

    public void showFuelLevel() {
        System.out.println("Fuel level: " + car.getFuel());
    }

    public void showStatus() {
        if (this.car.getEngine().isStarted()) {
            System.out.println("Car is started");
        } else {
            System.out.println("Car isn't started");
        }
    }
}