package com.zwt.buttons;
/**
 * @author ML李嘉图
 * @version createtime: 2021-11-10
 * Blog: https://www.cnblogs.com/zwtblog/
 */
/**
 * Common interface for all buttons.
 */
public interface Button {
    void render();

    void onClick();
}
