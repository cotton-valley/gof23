# gof23

## 介绍

学习网站推荐：

- https://refactoringguru.cn/design-patterns/catalog

- https://www.journaldev.com/1827/java-design-patterns-example-tutorial


算法更像是菜谱： 提供达成目标的明确步骤。 而模式更像是蓝图： 你可以看到最终的结果和模式的功能， 但需要自己确定实现步骤。


